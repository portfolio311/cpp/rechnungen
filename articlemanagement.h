#ifndef ARTICLEMANAGEMENT_H
#define ARTICLEMANAGEMENT_H

#include <QDialog>
#include <QList>
#include <QVariant>

namespace Ui {
class articlemanagement;
}

class articlemanagement : public QDialog
{
    Q_OBJECT

public:
    explicit articlemanagement(QWidget *parent = nullptr);
    ~articlemanagement();

private:
    Ui::articlemanagement *ui;

    int addedItems = 0;

public slots:
    void articlesLoaded(QList<QMap<QString, QVariant>>);
private slots:
    void on_btnAdd_clicked();

    void on_buttonBox_accepted();

signals:
    void addArticle(QString article, int vat, double price);
};

#endif // ARTICLEMANAGEMENT_H
