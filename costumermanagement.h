#ifndef COSTUMERMANAGEMENT_H
#define COSTUMERMANAGEMENT_H

#include <QDialog>
#include <QList>
#include <QVariant>

namespace Ui {
class costumerManagement;
}

class costumerManagement : public QDialog
{
    Q_OBJECT

public:
    explicit costumerManagement(QWidget *parent = nullptr);
    ~costumerManagement();
    int addedCustomers = 0;

private slots:
    void on_buttonBox_accepted();
    void customersLoaded(QList<QMap<QString, QVariant>>);
    void on_btnAdd_clicked();

private:
    Ui::costumerManagement *ui;

signals:
    void addCustomers(QString Vorname, QString Nachname, QString Strasse, QString Nummer, int PLZ, QString Ort);
};

#endif // COSTUMERMANAGEMENT_H
