#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "costumermanagement.h"
#include "invoicemanagement.h"
#include "articlemanagement.h"
#include "dbinterface.h"

#include <QDateTime>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    mCustomerManagement = new costumerManagement(this);
    mInvoiceManagement = new invoicemanagement(this);
    mArticleManagement = new articlemanagement(this);
    mDBInterface = new DBInterface();
    mDBInterface ->initDatabase();


    //Articles
    connect(mDBInterface, SIGNAL(articlesLoaded(QList<QMap<QString, QVariant>>)),
            mArticleManagement, SLOT(articlesLoaded(QList<QMap<QString, QVariant>>)));

    connect(mArticleManagement, SIGNAL(addArticle(QString, int, double)),
            mDBInterface, SLOT(addArticle(QString, int, double)));

    connect(mArticleManagement, SIGNAL(addArticle(QString, int, double)),
            mDBInterface, SLOT(getArticles()));

    //Customers
    connect(mDBInterface, SIGNAL(customersLoaded(QList<QMap<QString, QVariant>>)),
            mCustomerManagement, SLOT(customersLoaded(QList<QMap<QString, QVariant>>)));

    connect(mCustomerManagement, SIGNAL(addCustomers(QString, QString, QString, QString, int, QString)),
            mDBInterface, SLOT(addCustomer(QString, QString, QString, QString, int, QString)));

    connect(mCustomerManagement, SIGNAL(addCustomers(QString, QString, QString, QString, int, QString)),
            mDBInterface, SLOT(getCustomer()));

    //Invoice
    connect(mDBInterface, SIGNAL(articlesLoaded(QList<QMap<QString, QVariant>>)),
            mInvoiceManagement, SLOT(articlesLoaded(QList<QMap<QString, QVariant>>)));

    connect(mDBInterface, SIGNAL(customersLoaded(QList<QMap<QString, QVariant>>)),
            mInvoiceManagement, SLOT(customersLoaded(QList<QMap<QString, QVariant>>)));

    connect(mInvoiceManagement, SIGNAL(addInvoiceAndItem(int, int, QList<QMap<QString, int>>)),
            mDBInterface, SLOT(addInvoiceAndItems(int, int, QList<QMap<QString, int>>)));

    //MainWindow
    connect(mDBInterface, SIGNAL(invoiceItemsAndInvoiceAdded()), this, SLOT(updateTable()));

    updateTable();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btnAddCostumer_clicked()
{
    mCustomerManagement->show();
    mDBInterface->getCustomer();
}

void MainWindow::on_btnAddArticle_clicked()
{
    mArticleManagement->show();
    mDBInterface->getArticles();
}

void MainWindow::on_btnAddInvoice_clicked()
{
    mInvoiceManagement->show();
    mDBInterface->getArticles();
    mDBInterface->getCustomer();
}

void MainWindow::updateTable(){
    ui->tableRechnungen->clear();

    QStringList headers;
    headers.append("ReNr.");
    headers.append("Kunde");
    headers.append("Gesamtpreis");
    headers.append("Datum");
    ui->tableRechnungen->setColumnCount(4);
    ui->tableRechnungen->setHorizontalHeaderLabels(headers);
    ui->tableRechnungen->verticalHeader()->setVisible(false);

    QList<QMap<QString, QVariant>> Einkauf = mDBInterface->getEinkauf();
    QList<QMap<QString, QVariant>> Invoice = mDBInterface->getInvoice();
    QList<QMap<QString, QVariant>> Artikel = mDBInterface->getArticles();
    QList<QMap<QString, QVariant>> Kunde = mDBInterface->getCustomer();

    ui->tableRechnungen->setRowCount(Invoice.length());


    for(int i = 0; i < Invoice.length(); i++){
        int renr = Invoice.at(i).value("ReNr").toInt();
        int kdnr = Invoice.at(i).value("KdNr").toInt();
        QString name;

        for (int j = 0; j < Kunde.length(); j++) {
            int kdnrDB = Kunde.at(j).value("KdNr").toInt();

            if(kdnr == kdnrDB){
                name = Kunde.at(j).value("Vorname").toString() + " " + Kunde.at(j).value("Nachname").toString();
            }
        }

        double preis = 0.0;

        for (int j = 0; j < Einkauf.length(); j++) {
            if(Einkauf.at(j).value("ReNr") == renr){

                int artNr = Einkauf.at(j).value("ArtNr").toInt();

                for (int k = 0; k < Artikel.length(); k++) {
                    if(artNr == Artikel.at(k).value("ArtNr").toInt()){
                        preis = preis + (Artikel.at(k).value("Preis").toDouble() * Einkauf.at(j).value("Anzahl").toDouble());
                    }
                }
            }

        }

        QDateTime date = QDateTime::fromSecsSinceEpoch(Invoice.at(i).value("Datum").toInt());

        QTableWidgetItem *rechnung = new QTableWidgetItem(QString::number(renr));
        QTableWidgetItem *KundenName= new QTableWidgetItem(name);
        QTableWidgetItem *Preis = new QTableWidgetItem(QString::number(preis));
        QTableWidgetItem *Datum = new QTableWidgetItem(date.toString("yyyy-MM-dd hh:mm"));

        ui->tableRechnungen->setItem(i, 0, rechnung);
        ui->tableRechnungen->setItem(i, 1, KundenName);
        ui->tableRechnungen->setItem(i, 2, Preis);
        ui->tableRechnungen->setItem(i, 3, Datum);

        ui->tableRechnungen->resizeRowsToContents();
        ui->tableRechnungen->resizeColumnsToContents();
    }

}
