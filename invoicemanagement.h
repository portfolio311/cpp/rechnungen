#ifndef INVOICEMANAGEMENT_H
#define INVOICEMANAGEMENT_H

#include <QDialog>
#include <QList>
#include <QVariant>

namespace Ui {
class invoicemanagement;
}

class invoicemanagement : public QDialog
{
    Q_OBJECT

public:
    explicit invoicemanagement(QWidget *parent = nullptr);
    ~invoicemanagement();

private:
    Ui::invoicemanagement *ui;
public slots:
    void articlesLoaded(QList<QMap<QString, QVariant>>);
    void customersLoaded(QList<QMap<QString, QVariant>>);
private slots:
    void on_btnAdd_clicked();
    void on_buttonBox_accepted();
signals:
    void addInvoiceAndItem(int, int, QList<QMap<QString, int>>);
};

#endif // INVOICEMANAGEMENT_H
