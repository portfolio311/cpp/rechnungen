#ifndef DBINTERFACE_H
#define DBINTERFACE_H

#include <QObject>
#include <QtSql>
#include <QList>
#include <QMap>

class DBInterface: public QObject
{
    Q_OBJECT
public:
    //DBInterface();
    explicit DBInterface (QObject *parent = nullptr);
    bool initDatabase();

public slots:
    QList<QMap<QString, QVariant> > getArticles();
    QList<QMap<QString, QVariant> > getCustomer();
    QList<QMap<QString, QVariant> > getEinkauf();
    QList<QMap<QString, QVariant> > getInvoice();
    bool addArticle(QString article, int vat, double price);
    bool addCustomer(QString Vorname, QString Nachname, QString Strasse, QString Nummer, int PLZ, QString Ort);
    void addInvoiceAndItems(int kdnr, int date, QList<QMap<QString, int>> data);

signals:
    void articlesLoaded(QList<QMap<QString, QVariant>>);
    void customersLoaded(QList<QMap<QString, QVariant>>);
    void einkaufLoaded(QList<QMap<QString, QVariant>>);
    void invoiceLoaded(QList<QMap<QString, QVariant>>);

    void invoiceItemsAndInvoiceAdded();

private:
    QSqlDatabase mDb;
};

#endif // DBINTERFACE_H
