#include "invoicemanagement.h"
#include "ui_invoicemanagement.h"

#include <QDateTime>
#include <QDebug>
#include <QMessageBox>

invoicemanagement::invoicemanagement(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::invoicemanagement)
{
    ui->setupUi(this);

    QStringList headers;
    headers.append("ArtNr.");
    headers.append("Anzahl");
    headers.append("Artikel");
    ui->tableInvoice->setColumnCount(3);
    ui->tableInvoice->setHorizontalHeaderLabels(headers);
    ui->tableInvoice->verticalHeader()->setVisible(false);
}

invoicemanagement::~invoicemanagement()
{
    delete ui;
}

void invoicemanagement::articlesLoaded(QList<QMap<QString, QVariant>> data)
{
    ui->comboArticle->clear();
    for(int i = 0; i < data.size(); i++){
        QString article = data.at(i).value("Artikel").toString();

        ui->comboArticle->addItem(article);
        ui->comboArticle->setItemData(ui->comboArticle->count()-1, data.at(i).value("ArtNr"));
    }
}

void invoicemanagement::customersLoaded(QList<QMap<QString, QVariant>> data)
{
    ui->comboCostumer->clear();
    for(int i = 0; i < data.size(); i++){
        QString kdnr = QString::number(data.at(i).value("KdNr").toInt());
        QString vorname = data.at(i).value("Vorname").toString();
        QString nachname = data.at(i).value("Nachname").toString();

        ui->comboCostumer->addItem(kdnr + " " + vorname + " " + nachname);
        ui->comboCostumer->setItemData(ui->comboCostumer->count()-1, data.at(i).value("KdNr"));
    }
}

void invoicemanagement::on_btnAdd_clicked()
{
    QString artikel = ui->comboArticle->currentText();
    int artnr  = ui->comboArticle->currentData().toInt();
    int anzahl = ui->spinAmaunt->value();

    if(anzahl <= 0){
        QMessageBox messageBox;
        messageBox.critical(0, "Error", "Anzahl kann nicht 0 sein!");
        return;
    }

    int count = ui->tableInvoice->rowCount();
    ui->tableInvoice->setRowCount(count + 1);
    QTableWidgetItem *artItem = new QTableWidgetItem(QString::number(artnr));
    QTableWidgetItem *anzahlItem = new QTableWidgetItem(QString::number(anzahl));
    QTableWidgetItem *artikelItem = new QTableWidgetItem(artikel);
    ui->tableInvoice->setItem(count, 0, artItem);
    ui->tableInvoice->setItem(count, 1, anzahlItem);
    ui->tableInvoice->setItem(count, 2, artikelItem);

    ui->tableInvoice->resizeRowsToContents();
    ui->tableInvoice->resizeColumnsToContents();

}

void invoicemanagement::on_buttonBox_accepted()
{
    int kdnr;
    int date;
    QList<QMap<QString, int>> data;
    kdnr = ui->comboCostumer->currentData().toInt();
    date = QDateTime::currentDateTime().toSecsSinceEpoch();

    for(int i = 0; i < ui->tableInvoice->rowCount(); i++){
        int anzahl = ui->tableInvoice->item(i, 1)->text().toInt();
        int artnr = ui->tableInvoice->item(i, 0)->text().toInt();

        QMap<QString, int> tmp;
        tmp.insert("Anzahl", anzahl);
        tmp.insert("ArtNr", artnr);
        data.append(tmp);
    }

    if(ui->tableInvoice->rowCount() == 0){
        QMessageBox messageBox;
        messageBox.information(0, "Information", "Keine Rechnung hinzugefügt");
    }else{
        emit addInvoiceAndItem(kdnr, date, data);
    }

    ui->tableInvoice->clear();
}
