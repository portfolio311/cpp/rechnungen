#include "dbinterface.h"
#include <QtSql>

DBInterface::DBInterface(QObject *parent) : QObject(parent)
{

}

bool DBInterface::initDatabase(){
    mDb = QSqlDatabase::addDatabase("QSQLITE");
    mDb.setDatabaseName("02_Invoice.db");

    if(!mDb.open()){
        return false;
    }

    QStringList querries;
    querries.append("CREATE TABLE IF NOT EXISTS Artikel ("
                    "ArtNr INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                    "Artikel TEXT,"
                    "Preis REAL,"
                    "Ust INTEGER"
                    ")");   
    querries.append("CREATE TABLE IF NOT EXISTS  Kunde ("
                    "KdNr INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                    "Vorname TEXT,"
                    "Nachname TEXT,"
                    "Strasse TEXT,"
                    "Nummer TEXT,"
                    "PLZ INTEGER,"
                    "Ort TEXT"
                    ")");
    querries.append("CREATE TABLE IF NOT EXISTS  Rechnung ("
                    "ReNr INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                    "Datum INTEGER,"
                    "KdNr INTEGER,"
                    "FOREIGN KEY('KdNr') REFERENCES 'Kunde'('KdNr')"
                    ")");
    querries.append("CREATE TABLE IF NOT EXISTS  Einkauf ("
                    "ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
                    "ArtNr INTEGER,"
                    "Anzahl INTEGER,"
                    "ReNr INTEGER,"
                    "FOREIGN KEY('ArtNr') REFERENCES 'Artikel'('ArtNr'),"
                    "FOREIGN KEY('ReNr') REFERENCES 'Rechnung'('ReNr')"
                    ")");

    foreach(QString querryText, querries){
        QSqlQuery query;
        query.prepare(querryText);
        if(!query.exec()){
            qDebug() << "Error creating table";
            qDebug() << query.lastError();
            return false;
        }
    }

    return true;
}

QList<QMap<QString, QVariant>> DBInterface::getCustomer() {
    QList<QMap<QString, QVariant>> ret;

    QSqlQuery query;
    query.prepare("SELECT KdNr, Vorname, Nachname, Strasse, Nummer, PLZ, Ort FROM Kunde");
    if(!query.exec()){
        qDebug() << "Error running query";
        qDebug() << query.lastError();
        return ret;
    }

    while(query.next()){
        QMap<QString, QVariant> temp;
        temp.insert("KdNr", query.value("KdNr"));
        temp.insert("Vorname", query.value("Vorname"));
        temp.insert("Nachname", query.value("Nachname"));
        temp.insert("Strasse", query.value("Strasse"));
        temp.insert("Nummer", query.value("Nummer"));
        temp.insert("PLZ", query.value("PLZ"));
        temp.insert("Ort", query.value("Ort"));
        ret.append(temp);
    }
    emit customersLoaded(ret);
    return ret;
}

QList<QMap<QString, QVariant>> DBInterface::getArticles(){
    QList<QMap<QString, QVariant>> ret;

    QSqlQuery query;
    query.prepare("SELECT ArtNr, Artikel, Preis, Ust FROM Artikel");
    if(!query.exec()){
        qDebug() << "Error running query";
        qDebug() << query.lastError();
        return ret;
    }

    while(query.next()){
        QMap<QString, QVariant> temp;
        temp.insert("ArtNr", query.value("ArtNr"));
        temp.insert("Artikel", query.value("Artikel"));
        temp.insert("Preis", query.value("Preis"));
        temp.insert("Ust", query.value("Ust"));
        ret.append(temp);
    }
    emit articlesLoaded(ret);
    return ret;
}

QList<QMap<QString, QVariant>> DBInterface::getEinkauf(){
    QList<QMap<QString, QVariant>> ret;

    QSqlQuery query;
    query.prepare("SELECT ID, ArtNr, Anzahl, ReNr FROM Einkauf");

    if(!query.exec()){
        qDebug() << "Error running query";
        qDebug() << query.lastError();
        return ret;
    }

    while(query.next()){
        QMap<QString, QVariant> temp;
        temp.insert("ID", query.value("ID"));
        temp.insert("ArtNr", query.value("ArtNr"));
        temp.insert("Anzahl", query.value("Anzahl"));
        temp.insert("ReNr", query.value("ReNr"));
        ret.append(temp);
    }

    emit einkaufLoaded(ret);
    return ret;
}

QList<QMap<QString, QVariant>> DBInterface::getInvoice(){
    QList<QMap<QString, QVariant>> ret;

    QSqlQuery query;
    query.prepare("SELECT ReNr, Datum, KdNr FROM Rechnung");

    if(!query.exec()){
        qDebug() << "Error running query";
        qDebug() << query.lastError();
        return ret;
    }

    while(query.next()){
        QMap<QString, QVariant> temp;
        temp.insert("ReNr", query.value("ReNr"));
        temp.insert("Datum", query.value("Datum"));
        temp.insert("KdNr", query.value("KdNr"));
        ret.append(temp);
    }

    emit invoiceLoaded(ret);
    return ret;
}

bool DBInterface::addCustomer(QString Vorname, QString Nachname, QString Strasse, QString Nummer, int PLZ, QString Ort){
    QSqlQuery query;
    query.prepare("INSERT INTO Kunde (Vorname, Nachname, Strasse, Nummer, PLZ, Ort) "
                  "VALUES(:Vorname, :Nachname, :Strasse, :Nummer, :PLZ, :Ort)");

    query.bindValue(":Vorname", Vorname);
    query.bindValue(":Nachname", Nachname);
    query.bindValue(":Strasse", Strasse);
    query.bindValue(":Nummer", Nummer);
    query.bindValue(":Strasse", Strasse);
    query.bindValue(":PLZ", PLZ);
    query.bindValue(":Ort", Ort);

    if(!query.exec()){
        qDebug() << "Error inserting data";
        qDebug() << query.lastError();
        return false;
    }

    qDebug() << "Data inserted successfully";
    return true;
}

bool DBInterface::addArticle(QString article, int vat, double price){
    QSqlQuery query;
    query.prepare("INSERT INTO Artikel (Artikel, Preis, Ust) VALUES(:Artikel, :Preis, :Ust)");

    query.bindValue(":Artikel", article);
    query.bindValue(":Preis", price);
    query.bindValue(":Ust", vat);

    if(!query.exec()){
        qDebug() << "Error inserting data";
        qDebug() << query.lastError();
        return false;
    }

    qDebug() << "Data inserted successfully";
    return true;
}

void DBInterface::addInvoiceAndItems(int kdnr, int date, QList<QMap<QString, int> > data)
{
    QSqlQuery query;

    query.prepare("INSERT INTO Rechnung (Datum, KdNr) VALUES(:Datum, :KdNr)");

    query.bindValue(":Datum", date);
    query.bindValue(":KdNr", kdnr);

    if(!query.exec()){
        qDebug() << "Error inserting data";
        qDebug() << query.lastError();
        return;
    }

    int renr = query.lastInsertId().toInt();

    for(int i= 0; i < data.size(); i++){
        int artnr = data.at(i).value("ArtNr");
        int anzahl = data.at(i).value("Anzahl");
        query.clear();

        query.prepare("INSERT INTO Einkauf (ReNr, ArtNr, Anzahl) VALUES(:ReNr, :ArtNr, :Anzahl)");

        query.bindValue(":ReNr", renr);
        query.bindValue(":ArtNr", artnr);
        query.bindValue(":Anzahl", anzahl);

        if(!query.exec()){
            qDebug() << "Error inserting data";
            qDebug() << query.lastError();
            return;
        }
    }

    emit invoiceItemsAndInvoiceAdded();

}
