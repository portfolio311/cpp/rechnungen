#include "costumermanagement.h"
#include "ui_costumermanagement.h"

#include <QDebug>
#include <QMessageBox>

costumerManagement::costumerManagement(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::costumerManagement)
{
    ui->setupUi(this);
}

costumerManagement::~costumerManagement()
{
    delete ui;
}

void costumerManagement::on_buttonBox_accepted()
{
    QString Vorname;
    QString Nachname;
    QString Strasse;
    QString Nummer;
    int PLZ;
    QString Ort;

    int count = ui->tableCostumers->rowCount();
    int pos = count - 1;

    if(addedCustomers != 0){
        for (int i = 0; i < addedCustomers; i++) {
            Vorname = ui->tableCostumers->item(pos - i, 1)->text();
            Nachname = ui->tableCostumers->item(pos - i, 2)->text();
            Strasse = ui->tableCostumers->item(pos - i, 3)->text();
            Nummer = ui->tableCostumers->item(pos - i, 4)->text();
            PLZ = ui->tableCostumers->item(pos - i, 5)->text().toInt();
            Ort = ui->tableCostumers->item(pos - i, 6)->text();

            emit addCustomers(Vorname, Nachname, Strasse, Nummer, PLZ, Ort);
        }

        addedCustomers = 0;

    }else{
        QMessageBox messageBox;
        messageBox.information(0, "Information", "Keine Kunden hinzugefügt.");

    }

    ui->tableCostumers->clear();
}

void costumerManagement::customersLoaded(QList<QMap<QString, QVariant>> data){
    ui->tableCostumers->clear();
    ui->tableCostumers->setRowCount(data.length());
    ui->tableCostumers->setColumnCount(7);

    QStringList headers;
    headers.append("Kunden Nr.");
    headers.append("Vorname");
    headers.append("Nachname");
    headers.append("Straße");
    headers.append("Nummer");
    headers.append("PLZ");
    headers.append("Ort");
    ui->tableCostumers->setHorizontalHeaderLabels(headers);
    ui->tableCostumers->verticalHeader()->setVisible(false);

    for(int i = 0; i < data.length(); i++){
        QTableWidgetItem *kdnr = new QTableWidgetItem(data.at(i).value("KdNr").toString());
        ui->tableCostumers->setItem(i, 0, kdnr);

        QTableWidgetItem *vorname = new QTableWidgetItem(data.at(i).value("Vorname").toString());
        ui->tableCostumers->setItem(i, 1, vorname);

        QTableWidgetItem *nachname = new QTableWidgetItem(data.at(i).value("Nachname").toString());
        ui->tableCostumers->setItem(i, 2, nachname);

        QTableWidgetItem *strasse = new QTableWidgetItem(data.at(i).value("Strasse").toString());
        ui->tableCostumers->setItem(i, 3, strasse);

        QTableWidgetItem *nummer = new QTableWidgetItem(data.at(i).value("Nummer").toString());
        ui->tableCostumers->setItem(i, 4, nummer);

        QTableWidgetItem *plz = new QTableWidgetItem(data.at(i).value("PLZ").toString());
        ui->tableCostumers->setItem(i, 5, plz);

        QTableWidgetItem *ort = new QTableWidgetItem(data.at(i).value("Ort").toString());
        ui->tableCostumers->setItem(i, 6, ort);
    }

    ui->tableCostumers->resizeRowsToContents();
    ui->tableCostumers->resizeColumnsToContents();

}

void costumerManagement::on_btnAdd_clicked()
{
    QString Vorname = ui->editVorname->text();
    QString Nachname = ui->editNachname->text();
    QString Strasse = ui->editStrasse->text();
    QString Nummer = ui->editNummer->text();
    int PLZ = ui->editPlz->text().toInt();
    QString Ort = ui->editOrt->text();

    if(!Vorname.isEmpty() && !Nachname.isEmpty() && !Strasse.isEmpty() && !Nummer.isEmpty() && PLZ != 0 && !Ort.isEmpty()){
        int count = ui->tableCostumers->rowCount();
        ui->tableCostumers->setRowCount(count + 1);


        QTableWidgetItem *kdnr = new QTableWidgetItem("N/A");
        ui->tableCostumers->setItem(count, 0, kdnr);

        QTableWidgetItem *vorname = new QTableWidgetItem(Vorname);
        ui->tableCostumers->setItem(count, 1, vorname);

        QTableWidgetItem *nachname = new QTableWidgetItem(Nachname);
        ui->tableCostumers->setItem(count, 2, nachname);

        QTableWidgetItem *strasse = new QTableWidgetItem(Strasse);
        ui->tableCostumers->setItem(count, 3, strasse);

        QTableWidgetItem *nummer = new QTableWidgetItem(Nummer);
        ui->tableCostumers->setItem(count, 4, nummer);

        QTableWidgetItem *plz = new QTableWidgetItem(QString::number(PLZ));
        ui->tableCostumers->setItem(count, 5, plz);

        QTableWidgetItem *ort = new QTableWidgetItem(Ort);
        ui->tableCostumers->setItem(count, 6, ort);

        ui->editVorname->clear();
        ui->editNachname->clear();
        ui->editStrasse->clear();
        ui->editNummer->clear();
        ui->editPlz->clear();
        ui->editOrt->clear();

        addedCustomers++;

    }else{
        QMessageBox messageBox;
        messageBox.critical(0, "Error", "Alle Kundendaten müssen aufgefüllt werden!");
    }

}
