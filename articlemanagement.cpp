#include "articlemanagement.h"
#include "ui_articlemanagement.h"

#include <QDebug>
#include <QMessageBox>

articlemanagement::articlemanagement(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::articlemanagement)
{
    ui->setupUi(this);
}

articlemanagement::~articlemanagement()
{
    delete ui;
}

void articlemanagement::articlesLoaded(QList<QMap<QString, QVariant>> data){
    //qDebug() << data;

    ui->tableArticles->clear();
    ui->tableArticles->setRowCount(data.length());
    ui->tableArticles->setColumnCount(4);

    QStringList headers;
    headers.append("Art. Nr.");
    headers.append("Artikel");
    headers.append("Preis");
    headers.append("USt.");
    ui->tableArticles->setHorizontalHeaderLabels(headers);
    ui->tableArticles->verticalHeader()->setVisible(false);

    for(int i = 0; i < data.length(); i++){
        QTableWidgetItem *artnr = new QTableWidgetItem(data.at(i).value("ArtNr").toString());
        ui->tableArticles->setItem(i, 0, artnr);

        QTableWidgetItem *article = new QTableWidgetItem(data.at(i).value("Artikel").toString());
        ui->tableArticles->setItem(i, 1, article);

        QTableWidgetItem *price = new QTableWidgetItem(data.at(i).value("Preis").toString());
        ui->tableArticles->setItem(i, 2, price);

        QTableWidgetItem *ust = new QTableWidgetItem(data.at(i).value("Ust").toString());
        ui->tableArticles->setItem(i, 3, ust);
    }

    ui->tableArticles->resizeRowsToContents();
    ui->tableArticles->resizeColumnsToContents();

}

void articlemanagement::on_btnAdd_clicked()
{
    QString article = ui->editArticle->text();
    int vat = ui->comboUst->currentText().replace("%", "").toInt();
    double price = ui->spinPrice->value();

    int count = ui->tableArticles->rowCount();
    ui->tableArticles->setRowCount(count + 1);

    if(!article.isEmpty()){
        QTableWidgetItem *artnr = new QTableWidgetItem("N/A");
        ui->tableArticles->setItem(count, 0, artnr);

        QTableWidgetItem *Article = new QTableWidgetItem(article);
        ui->tableArticles->setItem(count, 1, Article);

        QTableWidgetItem *Price = new QTableWidgetItem(QString::number(price));
        ui->tableArticles->setItem(count, 2, Price);

        QTableWidgetItem *Ust = new QTableWidgetItem(QString::number(vat));
        ui->tableArticles->setItem(count, 3, Ust);

        ui->editArticle->setText("");
        ui->spinPrice->setValue(0);
        addedItems++;
    }else {
        QMessageBox messageBox;
        messageBox.critical(0,"Fehler", "Artikelname kann nicht leer sein!");
        messageBox.setFixedSize(500,200);
    }

}

void articlemanagement::on_buttonBox_accepted()
{
    QString article;
    int vat;
    double price;

    int count = ui->tableArticles->rowCount();
    int pos = count - 1;

    if(addedItems != 0){
        for (int i = 0; i < addedItems; i++) {
            article = ui->tableArticles->item(pos - i, 1)->text();
            price = ui->tableArticles->item(pos - i, 2)->text().toDouble();
            vat = ui->tableArticles->item(pos - i, 3)->text().toInt();
            //qDebug() << article;
            emit addArticle(article, vat, price);
        }
        addedItems = 0;
    }else{
        QMessageBox messageBox;
        messageBox.information(0, "Information", "Keine Artikel hinzugefügt.");
    }

    ui->tableArticles->clear();

}
