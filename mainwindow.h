#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QVariant>

class costumerManagement;
class invoicemanagement;
class articlemanagement;
class DBInterface;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_btnAddCostumer_clicked();
    void on_btnAddArticle_clicked();
    void on_btnAddInvoice_clicked();
    void updateTable();

private:
    Ui::MainWindow *ui;
    costumerManagement *mCustomerManagement;
    articlemanagement *mArticleManagement;
    invoicemanagement *mInvoiceManagement;
    DBInterface *mDBInterface;

};
#endif // MAINWINDOW_H
